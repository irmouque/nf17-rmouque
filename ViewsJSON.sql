CREATE VIEW vPerissable AS
SELECT * FROM Article
WHERE date_de_peremption IS NOT NULL;

CREATE VIEW vLivrable AS
SELECT a->>'date_achat' AS "date_achat", c.num_tel,a->>'id_tournee' as "idTournee", a->>'livraison_effectuee' as "Heure de Livraison"
FROM Client c, JSON_ARRAY_ELEMENTS(c.Achat) a
WHERE a->>'type' ='Livrable';

CREATE VIEW vRetirable AS
SELECT a->>'date_achat'AS "date_achat", num_tel, a->>'heure_retirable' AS "heure retirable", a->>'date_retrait' AS "heure retiré"
FROM Client c, JSON_ARRAY_ELEMENTS(c.Achat) a
WHERE a->>'type' ='Retirable';

CREATE VIEW vMontantTotal AS
SELECT c.num_tel, a->>'date_achat' AS "date_achat", sum(CAST(art->>'prix' AS INTEGER)) - sum(CAST(art->>'reduction' AS INTEGER))AS PRIX
FROM Client c,JSON_ARRAY_ELEMENTS(c.Achat) a, JSON_ARRAY_ELEMENTS(a->'Articles') art
GROUP BY c.num_tel, date_achat;


CREATE VIEW vTopVentes AS
SELECT art->>'nom' AS "nom", COUNT(*)
FROM Client c,JSON_ARRAY_ELEMENTS(c.Achat) a, JSON_ARRAY_ELEMENTS(a->'Articles') art
GROUP BY art->>'nom'
HAVING COUNT(*) > (
SELECT avg(co)
  FROM
    (
    SELECT art->>'nom' AS "nom", COUNT(*) as co
    FROM Client c,JSON_ARRAY_ELEMENTS(c.Achat) a, JSON_ARRAY_ELEMENTS(a->'Articles') art
    GROUP BY art->>'nom'
  ) as cs);

CREATE VIEW vBientotRupture AS
SELECT nom, COUNT(*)
FROM Article
GROUP BY nom
HAVING COUNT(*)<20;


CREATE VIEW vBienotPerime AS
SELECT id, nom, date_de_peremption
FROM Article
WHERE date_de_peremption < current_date + 7 	;
