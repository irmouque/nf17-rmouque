CREATE TABLE Client(
  nom VARCHAR(40)NOT NULL,
  prenom VARCHAR(40)NOT NULL,
  adresse VARCHAR(100) NOT NULL,
  num_tel VARCHAR(10) PRIMARY KEY,
  fidelite BOOLEAN NOT NULL,
  CHECK (num_tel SIMILAR TO '[0-9]{10}' and LENGTH(num_tel) = 10)
  );

CREATE TABLE Livreur(
  nom VARCHAR(40)NOT NULL,
  prenom VARCHAR(40)NOT NULL,
  adresse VARCHAR(100) NOT NULL,
  date_naissance DATE NOT NULL,
  num_tel VARCHAR(10) PRIMARY KEY,
  CHECK (num_tel SIMILAR TO '[0-9]{10}' and LENGTH(num_tel) = 10)
  );

CREATE TABLE Responsable_Catalogue(
  nom VARCHAR(40)NOT NULL,
  prenom VARCHAR(40)NOT NULL,
  adresse VARCHAR(100) NOT NULL,
  date_naissance DATE NOT NULL,
  num_tel VARCHAR(10) PRIMARY KEY,
  CHECK (num_tel SIMILAR TO '[0-9]{10}' and LENGTH(num_tel) = 10)
  );

CREATE TABLE Panier(
  num_tel VARCHAR(10) REFERENCES Client(num_tel),
  date_creation TIMESTAMP NOT NULL,
  PRIMARY KEY(date_creation, num_tel)
);

CREATE TABLE Ref_Produit(
  nom VARCHAR(40) PRIMARY KEY,
  prix NUMERIC NOT NULL CHECK (prix>0),
  reduction NUMERIC
);
--INT 4 force le fait que ce soit positif
CREATE TABLE Rayon(
  libelle VARCHAR(20) PRIMARY KEY
);

CREATE TABLE EstStockeDans(
  nom VARCHAR(40) REFERENCES Ref_Produit(nom),
  libelle VARCHAR(20) REFERENCES Rayon(libelle),
  PRIMARY KEY(nom, libelle)
);

CREATE TABLE Promotion(
  produit VARCHAR(40) REFERENCES Ref_Produit(nom),
  date_debut date,
  reduction NUMERIC CHECK (reduction>0),
  PRIMARY KEY(produit, date_debut));

CREATE TABLE Quantite(
  num_tel VARCHAR(10),
  date_creation TIMESTAMP,
  nom VARCHAR(40) REFERENCES Ref_Produit,
  qtt NUMERIC NOT NULL CHECK (qtt>0),
  PRIMARY KEY(num_tel, date_creation, nom),
  FOREIGN KEY(date_creation, num_tel) REFERENCES Panier(date_creation, num_tel)
);

Create TABLE Tournee(
  id int PRIMARY KEY,
  num_tel  VARCHAR(10) REFERENCES Livreur(num_tel),
  date_tournee date
);

CREATE TYPE TAchat AS ENUM ('Livrable', 'Retirable');



CREATE TABLE Achat(
  date_achat TIMESTAMP,
  num_tel VARCHAR(10) REFERENCES Client(num_tel),
  type TAchat NOT NULL,
  id_tournee INT REFERENCES Tournee(id),
  livraison_effectuee TIMESTAMP,--peut ne pa sêtre instancié tant que la commande n'a pas été livrée
  heure_retirable date,
  date_retrait TIMESTAMP,--peut ne pas être instancié tant que le client ne l'a pas retiré
  PRIMARY KEY (date_achat, num_tel),
  --  CHECK (NOT ((livraison_effectuee IS NOT NULL AND ((date_part('epoch', livraison_effectuee)-date_part('epoch', date_achat)) <0))),
  --  CHECK (NOT ((date_retrait IS NOT NULL AND DATEDIFF(date_achat, date_retrait)<0))), --je trouve pas où on voit a dans le cour
  CHECK (NOT (type = 'Livrable' AND (date_retrait IS NOT NULL OR heure_retirable IS NOT NULL OR id_Tournee IS NULL))),
  CHECK (NOT (type = 'Retirable' AND (livraison_effectuee IS NOT NULL OR id_tournee IS NOT NULL OR heure_retirable IS NULL)))
);



CREATE TABLE Article(
  id INT4 PRIMARY KEY,
  nom VARCHAR(40) REFERENCES Ref_Produit(nom) NOT NULL,
  date_achat timestamp,
  num_tel VARCHAR(10),
  date_de_peremption date,
  FOREIGN KEY (date_achat, num_tel) REFERENCES Achat(date_achat, num_tel)
);
