
INSERT INTO Client
VALUES ('rmouque', 'ilias', '18 rue des chenes 60000 Amiens', '0606060606', TRUE ),
('paul', 'lepoulpe', '7 avenue de la mer 69000 Lyon', '0606070502', FALSE );
--('moumou', 'lamouette', '12 route du vin 78000 Evry', '+33624242424', TRUE); --Mauvais numéro de téléphone

INSERT INTO Livreur
Values ('Chilly', 'Gonzales', '12 route du vin 76000 Quelquepart', to_date('1988-04-15', 'YYYY-MM-DD'), '0758486931' ),
('Chilly', 'Gonzales', '7 avenue de la mer 60000 Quelquepart', to_date('1988-04-15', 'YYYY-MM-DD'), '0654782413' );--il sont jumeaux et les parents n'étaient pas très inspirés

INSERT INTO Responsable_Catalogue
Values ('Michel', 'Dumas', '18 rue des chenes 67000 Strasbourg', to_date('1978-04-15', 'YYYY-MM-DD'), '0647895524');

INSERT INTO Panier
VALUES ('0606060606', to_timestamp('2020-02-07 11:10', 'YYYY-MM-DD HH:MI') );

INSERT INTO Ref_Produit
VALUES ('Tomate_500g_elGuapo', 150, 50),
('Chips_500g_Lays', 200, 20),
('Chips_250g_Lays', 100, 20),
('Table_de_jardin', 1400, NULL);

INSERT INTO Rayon
VALUES('Nourriture'),
('Apéro'),
('Meuble');

INSERT INTO EstStockeDans
VALUES('Chips_500g_Lays', 'Nourriture'),
('Chips_500g_Lays', 'Apéro'),
('Table_de_jardin', 'Meuble'),
('Tomate_500g_elGuapo','Nourriture');

INSERT INTO Promotion
VALUES ('Chips_500g_Lays',to_date('2020-03-08', 'YYYY-MM-DD'), 50 );

INSERT INTO Quantite
VALUES ('0606060606', to_timestamp('2020-02-07 11:10', 'YYYY-MM-DD HH:MI'), 'Chips_500g_Lays', 1),
('0606060606', to_timestamp('2020-02-07 11:10', 'YYYY-MM-DD HH:MI'), 'Tomate_500g_elGuapo', 2);

INSERT INTO Tournee
VALUES (1, '0758486931', to_date('2020-02-08', 'YYYY-MM-DD'));

INSERT INTO Achat
VALUES ( to_timestamp('2020-02-07 11:10', 'YYYY-MM-DD HH:MI'),'0606060606', 'Livrable', 1,to_date('2020-02-08 11:20', 'YYYY-MM-DD HH:MI'), NULL, NULL  ),
( to_timestamp('2020-02-07 11:10', 'YYYY-MM-DD HH:MI'), '0606070502', 'Retirable', NULL,NULL,  to_date('2020-02-08 10:20', 'YYYY-MM-DD HH:MI'), to_date('2020-02-08 11:20', 'YYYY-MM-DD HH:MI'));

INSERT INTO Article
VALUES (1, 'Chips_500g_Lays', to_timestamp('2020-02-07 11:10', 'YYYY-MM-DD HH:MI'),'0606060606',to_date('2020-08-07 ', 'YYYY-MM-DD')),
(2, 'Chips_500g_Lays', to_timestamp('2020-02-07 11:10', 'YYYY-MM-DD HH:MI'),'0606060606',to_date('2020-08-07', 'YYYY-MM-DD')),
(3, 'Tomate_500g_elGuapo', to_timestamp('2020-02-07 11:10', 'YYYY-MM-DD HH:MI'), '0606070502',to_date('2020-09-07 11:10', 'YYYY-MM-DD')),
(4, 'Table_de_jardin', to_timestamp('2020-02-07 11:10', 'YYYY-MM-DD HH:MI'), '0606070502',NULL),
(5, 'Chips_500g_Lays', NULL, NULL,to_date('2020-08-07', 'YYYY-MM-DD')),
(6, 'Table_de_jardin', NULL, NULL,NULL),
(7, 'Tomate_500g_elGuapo', NULL, NULL, to_date('2020-06-15', 'YYYY-MM-DD'));
