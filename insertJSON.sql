INSERT INTO Client
VALUES ('rmouque', 'ilias', '{"numéro":221,"rue":"rue du boulanger", "cp":60200, "ville":"Compiègne"}',
  '0606060606', TRUE,
'[
  [{"nom":"Chips_500g_Lays", "prix":200, "qtt": 2, "reduction":20}, {"nom":"Tomate_500g_elGuapo", "prix":150, "qtt": 1, "reduction":50}],
  [{"nom":"Chips_500g_Lays", "prix":200, "qtt": 2, "reduction":20}]
]',
'[
{
  "date_achat":"2020-02-07 11:10", "type": "Livrable", "livraison_effectuee":"2020-02-08 11:20", "Articles":
      [
        {"id": 1, "nom":"Chips_500g_Lays", "prix":200, "date_de_peremption":"2020-08-07", "reduction":0},
        {"id": 2, "nom":"Chips_500g_Lays", "prix":200, "date_de_peremption":"2020-08-07", "reduction":0},
        {"id": 3, "nom":"Tomate_500g_elGuapo", "prix":150, "date_de_peremption":"2020-09-07", "reduction":0}
      ]
}
]'),
('paul', 'lepoulpe', '{"numéro":7,"rue":"rue de la mer", "cp":69000, "ville":"Lyon"}', '0606070502', FALSE,
  '[]',
'[
  {
    "date_achat":"2020-03-08 11:10", "type": "Retirable", "heure_retirable":"2020-02-08 11:20", "date_retrait":"2020-03-08 11:10",  "Articles":
    [
    {"id": 6, "nom":"Table", "prix":1400, "reduction":0},
    {"id": 5, "nom":"Chips_500g_Lays","reduction":50, "prix":200, "date_de_peremption":"2020-08-07"}
    ]
  }
]');

INSERT INTO Livreur
Values ('Chilly', 'Gonzales', '{"numéro":7,"rue":"rue de la mer", "cp":69000, "ville":"Lyon"}', to_date('1988-04-15', 'YYYY-MM-DD'), '0758486931' ),
('Chilly', 'Gonzales', '{"numéro":5,"rue":"rue de la schlucht", "cp":67460, "ville":"Schwindratzheim"}', to_date('1988-04-15', 'YYYY-MM-DD'), '0654782413' );--il sont jumeaux et les parents n'étaient pas très inspirés

INSERT INTO Responsable_Catalogue
Values ('Michel', 'Dumas', '{"numéro":7,"rue":"rue de l amer", "cp":67500, "ville":"Niederschaeffolsheim"}', to_date('1978-04-15', 'YYYY-MM-DD'), '0647895524');

INSERT INTO Ref_Produit
VALUES ('Tomate_500g_elGuapo', 150, 50),
('Chips_500g_Lays', 200, 20),
('Chips_250g_Lays', 100, 20),
('Table_de_jardin', 1400, NULL);

INSERT INTO Rayon
VALUES('Nourriture'),
('Apéro'),
('Meuble');

INSERT INTO EstStockeDans
VALUES('Chips_500g_Lays', 'Nourriture'),
('Chips_500g_Lays', 'Apéro'),
('Table_de_jardin', 'Meuble'),
('Tomate_500g_elGuapo','Nourriture');

INSERT INTO Promotion
VALUES ('Chips_500g_Lays',to_date('2020-03-08', 'YYYY-MM-DD'), 50 );

INSERT INTO Tournee
VALUES (1, '0758486931', to_date('2020-02-08', 'YYYY-MM-DD'));

INSERT INTO Article
VALUES
(4, 'Table_de_jardin', NULL),
(7, 'Tomate_500g_elGuapo', to_date('2020-06-15', 'YYYY-MM-DD'));
