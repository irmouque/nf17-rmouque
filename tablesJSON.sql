CREATE TABLE Client(
  nom VARCHAR(40)NOT NULL,
  prenom VARCHAR(40)NOT NULL,
  adresse JSON NOT NULL,
  num_tel VARCHAR(10) PRIMARY KEY,
  fidelite BOOLEAN NOT NULL,
  Panier JSON NOT NULL,
  Achat JSON NOT NULL,
  CHECK (num_tel SIMILAR TO '[0-9]{10}' and LENGTH(num_tel) = 10)
  );

CREATE TABLE Livreur(
  nom VARCHAR(40)NOT NULL,
  prenom VARCHAR(40)NOT NULL,
  adresse JSON NOT NULL,
  date_naissance DATE NOT NULL,
  num_tel VARCHAR(10) PRIMARY KEY,
  CHECK (num_tel SIMILAR TO '[0-9]{10}' and LENGTH(num_tel) = 10)
  );

CREATE TABLE Responsable_Catalogue(
  nom VARCHAR(40)NOT NULL,
  prenom VARCHAR(40)NOT NULL,
  adresse JSON NOT NULL,
  date_naissance DATE NOT NULL,
  num_tel VARCHAR(10) PRIMARY KEY,
  CHECK (num_tel SIMILAR TO '[0-9]{10}' and LENGTH(num_tel) = 10)
  );



CREATE TABLE Ref_Produit(
  nom VARCHAR(40) PRIMARY KEY,
  prix NUMERIC NOT NULL CHECK (prix>0),
  reduction NUMERIC
);

CREATE TABLE Rayon(
  libelle VARCHAR(20) PRIMARY KEY
);

CREATE TABLE EstStockeDans(
  nom VARCHAR(40) REFERENCES Ref_Produit(nom),
  libelle VARCHAR(20) REFERENCES Rayon(libelle),
  PRIMARY KEY(nom, libelle)
);

CREATE TABLE Promotion(
  produit VARCHAR(40) REFERENCES Ref_Produit(nom),
  date_debut date,
  reduction NUMERIC CHECK (reduction>0),
  PRIMARY KEY(produit, date_debut));


Create TABLE Tournee(
  id int PRIMARY KEY,
  num_tel  VARCHAR(10) REFERENCES Livreur(num_tel),
  date_tournee date
);


CREATE TABLE Article(
  id INT4 PRIMARY KEY,
  nom VARCHAR(40) REFERENCES Ref_Produit(nom) NOT NULL,
  date_de_peremption date
);
