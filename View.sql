CREATE VIEW vPerissable AS
SELECT * FROM Article
WHERE date_de_peremption IS NOT NULL;

CREATE VIEW vLivrable AS
SELECT date_achat, num_tel,id_tournee, livraison_effectuee FROM Achat
WHERE type ='Livrable';

CREATE VIEW vRetirable AS
SELECT date_achat, num_tel, heure_retirable, date_retrait FROM Achat
WHERE type ='Retirable';

CREATE VIEW vMontantTotal AS
SELECT a.num_tel, a.date_achat, sum(r.prix)-  sum(
CASE
 WHEN p.date_debut < a.date_achat AND a.date_achat < p.date_debut + 30 THEN  p.reduction
 ELSE 0
END) AS "montantTotal"
FROM Article a, Ref_Produit r
LEft join Promotion p
ON p.produit = r.nom
WHERE r.nom = a.nom
AND a.num_tel IS NOT NULL and a.date_achat IS NOT NULL
GROUP BY a.num_tel, a.date_achat;

CREATE VIEW vTopVentes AS
SELECT art->>'nom' AS "nom", COUNT(*)
FROM Client c,JSON_ARRAY_ELEMENTS(c.Achat) a, JSON_ARRAY_ELEMENTS(a->'Articles') art
GROUP BY art->>'nom'
HAVING COUNT(*) > (
SELECT avg(co)
  FROM
    (
    SELECT COUNT (*) AS co
    FROM Client c,JSON_ARRAY_ELEMENTS(c.Achat) a, JSON_ARRAY_ELEMENTS(a->'Articles') art
GROUP BY art->>'nom'
  ) as cs);

-- change pas
CREATE VIEW vBientotRupture AS
SELECT nom, COUNT(*)
FROM Article
GROUP BY nom
HAVING COUNT(*)<20;

--change pas
CREATE VIEW vBienotPerime AS
SELECT id, nom, date_de_peremption
FROM Article
WHERE date_de_peremption < current_date + 7 	;
