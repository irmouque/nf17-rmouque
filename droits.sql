GRANT SELECT
      ON Achat, Panier, Ref_Produit
      TO Client;

GRANT SELECT
      ON Client, Achat, Tournee
      TO Livreur;
GRANT INSERT
      ON Achat, Tournee
      TO Livreur;

GRANT SELECT, UPDATE, INSERT, DELETE
      ON Article, Ref_Produit, Rayon,
      TO Responsable_Catalogue;
