
## Résumé
Suite à différents entretiens (cf en bas), à été retenu ce qui suit:

Les informations stockées sont:
Clients: nom, prénom, adresse, numtel
Livreurs : nom, prénom, date de naissance, adresse, numtel
Responsable_Catalogue: nom, prénom, date de naissance, adresse, numtel
Références_articles:nom, prix.
Perissable : date de péremption
Rayon: libellé.

pour le programme de fidélité, ne considérez que des points ajoutés sur une carte à point égaux à la somme des dépenses de l'utilisateur. => on peut donc le calculer à partir des achats  = méthode.


pour les stocks, vous devriez être capables de les retrouver par des requête => indique que chaque article doit avoir une instance, donc pas de gestion par référence comme je comptais faire initialement.


Une facture par contre référence des articles particuliers qui ont été
vendus. => gestion des articles à l'achat

## Détail des échanges:

### sur Mumble
Ok alors quelles informations voulez vous retenir sur les clients ?

Quelles informations voulez vous retenir sur les livreurs?

sur les produits ? notamment j'ai cru comprendre qu'il y avait deux types différents de produit les perissable ou non mis à part une date de péremption qu'est ce qui les différencie ?

Il est fait mentiion d'un responsable de catalogue, qui est il ? est-ce un employé ? le cas échéant dois-je traiter la gestion de l'ensemble des employés ou juste des livreurs et des responsable de catalogue ?

(édité)
d'ailleurs les rayons je prévoit juste de retenir un libellé est-ce suffisant pour vous ?


Benjamin Lussier
3:41 PM
nom, prénom, adresse, numtel
nom, prénom, date de naissance, adresse, numtel
nom, prix. à part la date de péremption, rien ne distingue les deux.
juste livreuer et responsable de catalogue.
a priori.

ilias_rmouque
3:42 PM
Merci

je n'ai pas compris ce qui vous intéressais au niveau du programme de fidelité.

et on ne gère pas les stocks pour les produits ?

Benjamin Lussier
3:44 PM
pour le programme de fidélité, ne considérez que des points ajoutés sur une carte à point égaux à la somme des dépenses de l'utilisateur.
pour les stocks, vous devriez être capables de les retrouver par des requêtes.

ilias_rmouque
3:45 PM
d'accord donc on gère les produits un a un et non comme un ensemble ?

les noms des produits sont uniques ?  

Benjamin Lussier
3:47 PM
il y a des choses que vous devez gérer un à un (la date de péremption par exemple). D'autres doivent être gérées en ensemble (le prix).

le nom est unique pour tous les articles de ce produit.

ilias_rmouque
3:48 PM
cela veut dire que deux produits peuvent porter le même nom ?

comment sont gérés les produits du mois ?

Benjamin Lussier
3:50 PM
Un produit est par exemple CHIPS_ONDULEE_MACHIN_100g. Un magasin vend bien sûr plusieurs paquets différents de ce même produit.

ilias_rmouque
3:50 PM
okay nickel merci


Benjamin Lussier
3:51 PM
Pour les produits du mois, un produit à une remise d'une valeur définie pendant tout un mois.


ilias_rmouque
3:52 PM
d'accord merci

on garde les mêmes infos pour livreur et responsable de catalogue ?


Benjamin Lussier
3:55 PM
oui

ilias_rmouque ge
ilias_rmouque
3:55 PM
merci

### Par Mail

Bonjour monsieur,

Je vous propose un résumé de ce que j’ai établis en fonction de nos
échanges précédents et vous invite à me réexpliciter tout point que
j’aurais mal compris (ou surinterprété) :

Un client peut composer un panier dans lequel il place des articles qui
correspondent à des références-produits, les références-produits sont
rangées dans un ou plusieurs rayons. Il existe deux types de produits
périssables ou non qui se différencient par leur date de péremption.

Une fois un panier enregistré, l’article ne peut plus être enregistré
dans un autre panier, de sorte qu’au moment de l’achat il n’ait pas déjà
été vendu. Un client peut acheter un panier qu’il a composé et choisir
la livraison ou le retrait en magasin, moment où il paiera ses achats.
Il a accès à l’historique de ses achats.

Un client peut souscrire ou non à un programme de fidélité qui lui
apportera un certain nombre de point calculable à partir de ses achats.

Il existe des promotions « produits du mois » qui réduisent le prix des
articles pendant un mois.

On gère également les livreurs, qui s’occupent des achats à livrer. On
souhaite pouvoir lister leurs tournées de livraison.
Vous souhaitez aussi enregistrer un responsable catalogue qui aurait une
vision des stocks des différentes références.
J’ai maintenant quelques questions :

* Le responsable catalogue est-il unique ? Et souhaitez vous qu’il soit
intégré à la base de données ?
* Comment par rapports au remises automatiques pour la fin de vie du
produit, comment souhaiter vous que cela soit mis   en place (un
pourcentage général, un pourcentage par référence …) ?
* Souhaitez-vous toujours gérer les ventes « Flash » ? Et si oui comment ?

Je vous remercie de l’attention que vous porterez à mon message et vous
souhaite une agréable semaine.

Cordialement,
Ilias Rmouque.


Bonjour,

Non, les paniers portent sur des types de produits, pas des articles
particuliers. Il n'y a pas de réservation d'article avec les paniers.
Une facture par contre référence des articles particuliers qui ont été
vendus.

Il y a deux types de promotion : produits du mois, et remise pour date
de péremption proche. La première se fait pendant un mois donné pour un
montant donné sur tous les articles d'un type de produit. Le deuxième se
fait pour des articles proches (moins de 7 jours) de la date de péremption.
  * Le responsable catalogue est-il unique ? Et souhaitez vous qu’il
soit intégré à la base de données ?
Il n'est pas unique, et représentez les comme les livreurs.
* Comment par rapports au remises automatiques pour la fin de vie du
produit, comment souhaiter vous que cela soit mis   en place (un
pourcentage général, un pourcentage par référence …) ?
La remise est la même sur tous les articles de même type. Comme dit
au-dessus, elle est effective si la date de péremption est dans moins de
7 jours.
* Souhaitez-vous toujours gérer les ventes « Flash » ? Et si oui
comment ?
Non, disons que c'est fait au niveau applicatif à partir des remises
mentionnées.

Cordialement,
  -- Benjamin Lussier



suite

Les compositions entre client et panier et panier et ref_produit sont
inversées.

Achat ne devrait pas être dupliqué, mais juste dans un attribut json
dans client, avec par contre une version dupliquée de
article/ref_produit attaché pour permettre de savoir ce qui a été acheté
et pour pouvoir calculer le prix).

Essayiez de toutes façons de passer à la suite, pour voir ce qui
accorche ou pas.

Cordialement,
  -- Benjamin Lussier


On 6/12/2020 3:21 PM, ilias.rmouque@etu.utc.fr wrote:

[Cacher les citations]
AVEC les pièces jointes c'est mieux
ilias.rmouque@etu.utc.fr <mailto:ilias.rmouque@etu.utc.fr> a écrit :
Bonjour,

Est-ce que ce MCD (UmlAprès3) vous convient ?
Je ne sais pas si je représente comme il faut ce que je compte faire,
mais en gros les classes passés en Json j'ai rajouté DUP derrière et
je les mets au format json dans la classe qui va les contenir. Je suis
pas très sûr au niveau de "rayon" dans produit dupliqué c'est ce que
j'ai compris de votre mail "(rajouter les classes panier/produit/rayon
correspondant au panier)".

Est-ce que je peux passer à la suite ?


Merci d'avance pour votre réponse.
Cordialement,
Ilias Rmouque.




Benjamin Lussier <benjamin.lussier@hds.utc.fr
<mailto:benjamin.lussier@hds.utc.fr>> a écrit :
Bonjour,

J'avais demandé de faire également achat en json dans client. Je vous
autorise à ne pas tenir compte de l'association entre livrable et
tournee (ou à mettre la clé de tournée à la main dans le json). Mettez
bien sûr la liste des articles et ref_produits concernés dans le json.

Représentez sur l'UML les classes dupliquées pour que je puisse mieux
comprendre ce que vous faites (rajouter les classes panier/produit/rayon
correspondant au panier). Je ne vous demande pas de mettre rayon et
promotion en json pour la table ref_produit. Ne faites ça que pour ce
qui est déjà en json (panier, donc).

Cordialement,
-- Benjamin Lussier


On 6/12/2020 1:22 PM, ilias.rmouque@etu.utc.fr
<mailto:ilias.rmouque@etu.utc.fr> wrote:
Bonjour,

Du coup, entre UMLAprès et UMLAprès2 (joint),  j'ai rappatrié dans Rayon
Promotion et Rayon, et dans client j'ai rappatrié panier au format JSON,
est-ce que cela est suffisant pour vous ?

Merci d'avance pour votre réponse.
Cordialement,
Ilias Rmouque.

Benjamin Lussier <benjamin.lussier@hds.utc.fr
<mailto:benjamin.lussier@hds.utc.fr>
<mailto:benjamin.lussier@hds.utc.fr>>
<mailto:benjamin.lussier@hds.utc.fr>>> a écrit :
Bonjour,

C'est la deuxième solution Postgresql/JSON en me faisant valider ce que
vous comptez faire. Il vous reste moins de 12H pour le faire, bon
courage.

Cordialement,
-- Benjamin Lussier



On 6/12/2020 11:53 AM, ilias.rmouque@etu.utc.fr
<mailto:ilias.rmouque@etu.utc.fr>
<mailto:ilias.rmouque@etu.utc.fr>
<mailto:ilias.rmouque@etu.utc.fr>> wrote:
Bonjour Monsieur,

C'est encore moi !.. Je vous avoue que j'ai un peu du mal à comprendre
exactement ce qu'il faut faire pour cette semaine, est-ce que vous
attendez de nous qu'on refasse complètement notre base de données en
mode NOSQL ou est-ce qu'on doit profiter du fait que postgre propose
d'intégrer des éléments de JSON pour l'intégrer aux endroits ou cela
nous semble pertinent ?

Je vous avoue que plus nous échangeons, moins  j'ai l'impression de
comprendre ce qui est attendu de nous...

Merci d'avance pour votre réponse.
Cordialement,
Ilias Rmouque.



Benjamin Lussier <benjamin.lussier@hds.utc.fr
<mailto:benjamin.lussier@hds.utc.fr>
<mailto:benjamin.lussier@hds.utc.fr>
<mailto:benjamin.lussier@hds.utc.fr>>
<mailto:benjamin.lussier@hds.utc.fr>>
<mailto:benjamin.lussier@hds.utc.fr>>>
<mailto:benjamin.lussier@hds.utc.fr>>>
<mailto:benjamin.lussier@hds.utc.fr>>>> a écrit :
Bonjour,

C'est l'idée, mais sur le MCD reprenez toutes les classes et
montrez les
duplications et les compositions. Par exemple, panier a une
composition
avec une classe dupliquée de prosuit, ce dernier ayant une
composition
avec une classe dupliquée de rayon. De la même façon, achat a une
composition avec article, qui a une composition avec produit, etc.

Pour la requête, affichez les produits ayant été plus vendus que la
moyenne.

Cordialement,
-- Benjamin Lussier

On 6/9/2020 2:15 PM, ilias.rmouque@etu.utc.fr
<mailto:ilias.rmouque@etu.utc.fr>
<mailto:ilias.rmouque@etu.utc.fr> <mailto:ilias.rmouque@etu.utc.fr>>
<mailto:ilias.rmouque@etu.utc.fr>
<mailto:ilias.rmouque@etu.utc.fr>>
<mailto:ilias.rmouque@etu.utc.fr>>
<mailto:ilias.rmouque@etu.utc.fr>>>
wrote:
Bonjour,

Voilà l'évolution de mon mcd pour y inclure les éléments sous
forme de
JSON: au niveau de adresse j'avoue (honteusement) que cela reprend
l'exemple du cours mais il me semble qu'ici c'est relativement
pertinent.
Ensuite au niveau de Panier l'ajout du champ multivalué produit
permet
d'y placer plusieurs référence de produits, bien que cela
introduise de
la redondance, on peut tout à fait gérer au niveau applicatif la
contraite de référence à ref_produit.

J'espère avoir compris ce qu'il faut faire et pas répondre
complètement
à côté. :)

Egalement pour le point des produits les plus vendus: une liste de
l'ensemble des produits par ordre décroissant du nombre de
ventes vous
conviendrais-t-elle ?

Merci d'avance pour votre réponse.

Cordialement,
Ilias Rmouque.


Benjamin Lussier <benjamin.lussier@hds.utc.fr
<mailto:benjamin.lussier@hds.utc.fr>
<mailto:benjamin.lussier@hds.utc.fr>
<mailto:benjamin.lussier@hds.utc.fr>>
<mailto:benjamin.lussier@hds.utc.fr>
<mailto:benjamin.lussier@hds.utc.fr>>
<mailto:benjamin.lussier@hds.utc.fr>>
<mailto:benjamin.lussier@hds.utc.fr>>>
<mailto:benjamin.lussier@hds.utc.fr>>
<mailto:benjamin.lussier@hds.utc.fr>>>
<mailto:benjamin.lussier@hds.utc.fr>>>
<mailto:benjamin.lussier@hds.utc.fr>>>>
<mailto:benjamin.lussier@hds.utc.fr>>>
<mailto:benjamin.lussier@hds.utc.fr>>>>
<mailto:benjamin.lussier@hds.utc.fr>>>>
<mailto:benjamin.lussier@hds.utc.fr>>>>> a écrit :
Bonjour,

Envoyez-moi un MCD montrant vos compositions.

Pour les requêtes :
1. Quels sont les produits les plus vendus.
2. Quels sont les produits qui sont à moins de 7 jours de leur
date de
péremption.
3. Quels sont les produits dont les stocks (produits non vendus
et non
périmés) sont inférieur à 20.

Cordialement,
-- Benjamin Lussier



On 6/8/2020 3:12 PM, ilias.rmouque@etu.utc.fr
<mailto:ilias.rmouque@etu.utc.fr>
<mailto:ilias.rmouque@etu.utc.fr>
<mailto:ilias.rmouque@etu.utc.fr>>
<mailto:ilias.rmouque@etu.utc.fr>
<mailto:ilias.rmouque@etu.utc.fr>>
<mailto:ilias.rmouque@etu.utc.fr>>
<mailto:ilias.rmouque@etu.utc.fr>>>
<mailto:ilias.rmouque@etu.utc.fr>
<mailto:ilias.rmouque@etu.utc.fr>>
<mailto:ilias.rmouque@etu.utc.fr>>
<mailto:ilias.rmouque@etu.utc.fr>>>
<mailto:ilias.rmouque@etu.utc.fr>>
<mailto:ilias.rmouque@etu.utc.fr>>>
<mailto:ilias.rmouque@etu.utc.fr>>>
<mailto:ilias.rmouque@etu.utc.fr>>>>
wrote:
Bonjour suite à notre entretien:

Je pense que adresse dans Personne est un bon attribut à passé en
JSON
sous forme d'attribut composé. Ensuite je pense que ça peut être
intéressant de rapattrier Quantité dans panier soit sous la
forme :
["Chips_Lays", "Chips_Lays", "Chips_Lays", "autre_chose"] soit
sous la
forme: [{"nom":"Chips_Lays", "qtt":3}, { "nom":"autre_chose",
"qtt":1}].

Au niveau des requettes ensuite:

  *

    Les outils statistiques sont importants : produits les plus
vendus,
    produits bientôt périmés, produits bientôt en rupture...

    Bon une fois de plus le sujet est très flou et je voudrais pas
    perdre encore des points pour avoir pris des intiatives non
    jusitifiées, est-ce que vous pouvez me le reformuler de
manière non
    ambigüe et faisable en SQL svp ?

    Cordialement,
    Ilias Rmouque.
 
